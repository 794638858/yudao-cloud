package cn.iocoder.yudao.framework.env.core.web;

import cn.hutool.core.util.StrUtil;
import cn.iocoder.yudao.framework.env.core.context.EnvContextHolder;
import cn.iocoder.yudao.framework.env.core.util.EnvUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 环境的 {@link javax.servlet.Filter} 实现类
 * 当有 tag 请求头时，设置到 {@link EnvContextHolder} 的标签上下文
 *OncePerRequestFilter是Spring框架中一个非常有用的工具，它可以帮助你在一次完整的HTTP请求中仅执行一次过滤器的逻辑。
 * 通过继承OncePerRequestFilter并重写doFilterInternal方法，你可以轻松地创建自定义的过滤器来处理各种需求。
 * 无论是在记录请求处理时间、设置请求属性、进行权限验证等方面，OncePerRequestFilter都能为你提供强大的支持。
 * @author 芋道源码
 */
public class EnvWebFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {
        // 如果没有 tag，则走默认的流程
        String tag = EnvUtils.getTag(request);
        if (StrUtil.isEmpty(tag)) {
            chain.doFilter(request, response);
            return;
        }

        // 如果有 tag，则设置到上下文
        EnvContextHolder.setTag(tag);
        try {
            chain.doFilter(request, response);
        } finally {
            EnvContextHolder.removeTag();
        }
    }

}
